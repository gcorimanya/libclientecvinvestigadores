package pe.gob.concytec.investigadores.cliente;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import pe.gob.concytec.investigadores.cliente.util.ConfiguracionUtil;
import pe.gob.concytec.investigadores.cliente.util.GenericRestClient;

public class ClienteCVInvestigadores {
	public static void main(String args[]){
		try {
			ByteArrayOutputStream output = ClienteCVInvestigadores.getCVInvestigador("1","10220021");
			 FileOutputStream outTemp = new FileOutputStream(new File("c:/temp/"+"nombreArchivo1206.pdf"));
			 
			 System.out.println("Salida: "+output.toByteArray());
			 
			 outTemp.write(output.toByteArray());
		     outTemp.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static ByteArrayOutputStream getCVInvestigador(String tipoDocumento, String nroDocumento) throws Exception{
		
		String strURL=ConfiguracionUtil.obtenerValor("web.service.investigadores.url");
		strURL+=tipoDocumento+"/"+nroDocumento;
    	String strRequestMethod = "GET";
    	String strType="application/json";
    	
    	
    	System.out.println("strURL:"+strURL);
    	/*CriterioInstitucion objCriterioInstitucion = new CriterioInstitucion();
    	objCriterioInstitucion.setRuc(ruc!=null?""+ruc:null);
    	objCriterioInstitucion.setRazonSocial(nombreInstitucion);
    	objCriterioInstitucion.setIdTipoInstitucion(idTipoInstitucion);
    	*/
    	
    	
    	String strRequest = null;
    	//System.out.println("strRequest:"+strRequest);
    	
    	ByteArrayOutputStream output = GenericRestClient.consumeRestServiceByteArray(strURL, strRequestMethod, strType, strRequest,ConfiguracionUtil.obtenerValor("web.service.user"),ConfiguracionUtil.obtenerValor("web.service.password") );
		
		
		return output;
	}
	
	
}
