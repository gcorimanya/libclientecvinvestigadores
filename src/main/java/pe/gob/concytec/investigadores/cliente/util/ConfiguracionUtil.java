package pe.gob.concytec.investigadores.cliente.util;

import java.util.ResourceBundle;

public class ConfiguracionUtil {
	private static String NAME_BUNDLE = "config_cv";
	private static ResourceBundle objRecurso = ResourceBundle.getBundle(NAME_BUNDLE);
	public static String obtenerValor(String strPropiedad){
		String strRetorno = null;
		strRetorno = objRecurso.getString(strPropiedad);
		return strRetorno;
	}
}
