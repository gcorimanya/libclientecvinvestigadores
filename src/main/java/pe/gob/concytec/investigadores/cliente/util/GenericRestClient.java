package pe.gob.concytec.investigadores.cliente.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;



public class GenericRestClient {
	//private static Client client = Client.create();
	//private static WebResource eSearchResource = null;
	public static void main(String[] args)
    {
        try
        {
            
        	String strURL="http://localhost:9093/service/instituciones/consulta/";
        	String strRequestMethod = "POST";
        	String strType="application/json";
        	/*MultivaluedMap<String, String> searchParams = new MultivaluedMapImpl();
        	searchParams.add("db", "pubmed");
        	searchParams.add("term", "Curioso%20Walter[FAU]");
        	searchParams.add("retstart", "0");
        	searchParams.add("retmax", "20");
        	*/
        	String strResultado=null;
        	strResultado = consumeRestService(strURL, strRequestMethod, strType, "{\"razonSocial\":\"process\"}","usrConcytec","concytec123");
			/*try {				
				strResultado = search(strURL, searchParams);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        	System.out.println(strResultado);
            
        	
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
	
	
	
	
	
	
	public static String consumeRestService(String strURL, String strRequestMethod, String strType, String strRequestPost, String strUser, String strPassword)throws MalformedURLException, IOException{
		String strRetorno = null;
		URL url = new URL(strURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        Base64 objBase64 = new Base64();
        String encoded = objBase64.encodeToString((strUser + ":" + strPassword).getBytes("UTF-8")); 
        conn.setRequestProperty("Authorization", "Basic "+encoded);
        
        conn.setDoOutput(true);
        conn.setDoInput(true);
        
        conn.setRequestMethod(strRequestMethod);
        conn.setRequestProperty("Content-Type", strType);
        conn.setRequestProperty("Accept", strType);
        if(strRequestPost!=null){
	        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	        System.out.println("strRequestPost:"+strRequestPost);
	        wr.write(strRequestPost);
	        wr.flush();
        }

        if (conn.getResponseCode() != 200)
        {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        
        
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()),"UTF-8"));
        String apiOutput = "";
		String strTemp = br.readLine();
		while(strTemp!=null){
			if(apiOutput.equals(""))
				apiOutput = strTemp;
			else
				apiOutput = apiOutput + "\n"+strTemp;
			strTemp = br.readLine();
		}
		
        strRetorno=apiOutput;
        conn.disconnect();
        
        return strRetorno;
	}
	
	public static ByteArrayOutputStream consumeRestServiceByteArray(String strURL, String strRequestMethod, String strType, String strRequestPost, String strUser, String strPassword)throws MalformedURLException, IOException{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		URL url = new URL(strURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        Base64 objBase64 = new Base64();
        String encoded = objBase64.encodeToString((strUser + ":" + strPassword).getBytes("UTF-8")); 
        conn.setRequestProperty("Authorization", "Basic "+encoded);
        
        conn.setDoOutput(true);
        conn.setDoInput(true);
        
        conn.setRequestMethod(strRequestMethod);
        conn.setRequestProperty("Content-Type", strType);
        //conn.setRequestProperty("Accept", strType);
        if(strRequestPost!=null){
	        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	        System.out.println("strRequestPost:"+strRequestPost);
	        wr.write(strRequestPost);
	        wr.flush();
        }

        if (conn.getResponseCode() != 200)
        {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        
        //InputStreamReader input = new InputStreamReader((conn.getInputStream()),"UTF-8");
        byte[] buffer = new byte[8192];
        int bytesRead;
        
        while ((bytesRead = conn.getInputStream().read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
        /*BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()),"UTF-8"));
        String apiOutput = "";
		String strTemp = br.readLine();
		while(strTemp!=null){
			if(apiOutput.equals(""))
				apiOutput = strTemp;
			else
				apiOutput = apiOutput + "\n"+strTemp;
			strTemp = br.readLine();
		}
		
        strRetorno=apiOutput;
        */
        conn.disconnect();
        
        return output;
	}
	/*
	public static String search(String strURL, MultivaluedMap<String, String> queryParams) throws JAXBException, IOException {
		String strRetorno;		
		eSearchResource = client.resource(strURL);
		InputStream is = eSearchResource.queryParams(queryParams).get(InputStream.class);
		BufferedReader br = new BufferedReader(new InputStreamReader(is,"UTF-8"));
		String apiOutput = "";
		String strTemp = "";
		while(strTemp!=null){
			strTemp = br.readLine();
			apiOutput = apiOutput + "\n"+strTemp;
		}
		
        strRetorno=apiOutput;
		//ESearchResult searchResult = (ESearchResult) searchUnmarshaller.unmarshal(is);
		try {
			is.close();
		} catch (IOException e) {
		
		}
		
		return strRetorno;
	}
	*/
}
